Terraform Docker container

with the [kong provider plugin](https://github.com/kevholditch/terraform-provider-kong)

Because we're using this plugin, you need to mount your tf templates in /opt/tf as per examples below

# Usage

`docker run -ti --rm rdkls/terraform_kong <command>`

e.g.

```
docker run --rm -ti -v $(pwd):/opt/tf rdkls/terraform_kong init
docker run --rm -ti -v $(pwd):/opt/tf rdkls/terraform_kong apply
```
